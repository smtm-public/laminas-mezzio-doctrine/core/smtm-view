<?php

declare(strict_types=1);

namespace Smtm\View;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-view')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-view');
    $dotenv->load();
}

return [
    'default404ContentTypeText' => filter_var(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_VIEW_DEFAULT_404_CONTENT_TYPE_TEXT'),
        FILTER_VALIDATE_BOOLEAN
    ),
];
