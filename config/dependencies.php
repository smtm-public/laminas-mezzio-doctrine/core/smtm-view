<?php

declare(strict_types=1);

namespace Smtm\View;


use Smtm\View\Http\Middleware\Factory\NotFoundHandlerDelegator;
use Smtm\View\Infrastructure\Laminas\Log\Writer\MezzioTemplateRenderer;
use Smtm\View\Infrastructure\Mezzio\Template\Factory\TemplateRendererAwareDelegator;
use Laminas\Log\Writer\Factory\WriterFactory;
use Laminas\Log\WriterPluginManager;
use Psr\Container\Containerinterface;
use Mezzio\Handler\NotFoundHandler;

return [
    'delegators' => [
        NotFoundHandler::class => [
            NotFoundHandlerDelegator::class,
        ],
        'LogWriterManager' => [
            function (ContainerInterface $container, $name, callable $callback, array $options = null) {
                /** @var WriterPluginManager $object */
                $object = $callback();

                $object->setFactory(
                    MezzioTemplateRenderer::class,
                    WriterFactory::class
                );

                $object->addDelegator(
                    MezzioTemplateRenderer::class,
                    TemplateRendererAwareDelegator::class
                );

                $object->setShared(MezzioTemplateRenderer::class, false);

                return $object;
            }
        ],
    ],
];
