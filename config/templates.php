<?php

declare(strict_types=1);

namespace Eduspire\Common;

return [
    'paths' => [
        'error'  => [__DIR__ . '/../template/error'],
        'layout' => [__DIR__ . '/../template/layout'],
        'element'    => [__DIR__ . '/../template/element'],
    ],
];
