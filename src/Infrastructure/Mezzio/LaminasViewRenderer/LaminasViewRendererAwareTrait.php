<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Mezzio\LaminasViewRenderer;

use Mezzio\LaminasView\LaminasViewRenderer;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait LaminasViewRendererAwareTrait
{
    protected LaminasViewRenderer $laminasViewRenderer;

    public function getTemplateRenderer(): LaminasViewRenderer
    {
        return $this->laminasViewRenderer;
    }

    public function setTemplateRenderer(LaminasViewRenderer $laminasViewRenderer): static
    {
        $this->laminasViewRenderer = $laminasViewRenderer;

        return $this;
    }
}
