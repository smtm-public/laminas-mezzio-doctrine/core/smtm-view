<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Mezzio\LaminasViewRenderer;

use Mezzio\LaminasView\LaminasViewRenderer;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface LaminasViewRendererAwareInterface
{
    public function getLaminasViewRenderer(): LaminasViewRenderer;
    public function setLaminasViewRenderer(LaminasViewRenderer $laminasViewRenderer): static;
}
