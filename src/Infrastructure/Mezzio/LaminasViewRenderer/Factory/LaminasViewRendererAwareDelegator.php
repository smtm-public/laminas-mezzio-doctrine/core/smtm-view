<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Mezzio\LaminasViewRenderer\Factory;

use Smtm\View\Infrastructure\Mezzio\LaminasViewRenderer\LaminasViewRendererAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Mezzio\LaminasView\LaminasViewRenderer;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class LaminasViewRendererAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var LaminasViewRendererAwareInterface $object */
        $object = $callback();

        $object->setLaminasViewRenderer($container->get(LaminasViewRenderer::class));

        return $object;
    }
}
