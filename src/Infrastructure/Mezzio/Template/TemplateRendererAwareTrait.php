<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Mezzio\Template;

use Mezzio\Template\TemplateRendererInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait TemplateRendererAwareTrait
{
    protected TemplateRendererInterface $templateRenderer;

    public function getTemplateRenderer(): TemplateRendererInterface
    {
        return $this->templateRenderer;
    }

    public function setTemplateRenderer(TemplateRendererInterface $templateRenderer): static
    {
        $this->templateRenderer = $templateRenderer;

        return $this;
    }
}
