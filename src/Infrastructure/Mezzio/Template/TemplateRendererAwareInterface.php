<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Mezzio\Template;

use Mezzio\Template\TemplateRendererInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TemplateRendererAwareInterface
{
    public function getTemplateRenderer(): TemplateRendererInterface;
    public function setTemplateRenderer(TemplateRendererInterface $templateRenderer): static;
}
