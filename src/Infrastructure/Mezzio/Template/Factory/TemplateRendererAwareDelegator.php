<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Mezzio\Template\Factory;

use Smtm\View\Infrastructure\Mezzio\Template\TemplateRendererAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TemplateRendererAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var TemplateRendererAwareInterface $object */
        $object = $callback();

        $object->setTemplateRenderer($container->get(TemplateRendererInterface::class));

        return $object;
    }
}
