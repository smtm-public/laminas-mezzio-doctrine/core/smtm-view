<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Laminas\View\Renderer\Factory;

use Smtm\View\Infrastructure\Laminas\View\Renderer\RendererAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Laminas\View\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RendererAwareDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var RendererAwareInterface $object */
        $object = $callback();

        $object->setRenderer($container->get(RendererInterface::class));

        return $object;
    }
}
