<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Laminas\View\Renderer;

use Laminas\View\Renderer\RendererInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RendererAwareInterface
{
    public function getRenderer(): RendererInterface;
    public function setRenderer(RendererInterface $templateRenderer): static;
}
