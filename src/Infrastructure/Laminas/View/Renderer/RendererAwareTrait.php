<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Laminas\View\Renderer;

use Laminas\View\Renderer\RendererInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait RendererAwareTrait
{
    protected RendererInterface $renderer;

    public function getRenderer(): RendererInterface
    {
        return $this->renderer;
    }

    public function setRenderer(RendererInterface $renderer): static
    {
        $this->renderer = $renderer;

        return $this;
    }
}
