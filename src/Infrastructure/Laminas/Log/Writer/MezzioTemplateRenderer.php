<?php

declare(strict_types=1);

namespace Smtm\View\Infrastructure\Laminas\Log\Writer;

use Smtm\View\Infrastructure\Mezzio\Template\TemplateRendererAwareInterface;
use Smtm\View\Infrastructure\Mezzio\Template\TemplateRendererAwareTrait;
use Laminas\Log\Exception;
use Laminas\Log\Formatter\Simple as SimpleFormatter;
use Laminas\Log\Writer\AbstractWriter;
use Laminas\View\Model\ViewModel;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MezzioTemplateRenderer extends AbstractWriter implements TemplateRendererAwareInterface
{

    use TemplateRendererAwareTrait;

    protected string $templateLayoutName;
    protected string $templateContentName;
    protected string $fileName;

    public function __construct(
        $options = null
    ) {
        if ($options['templateLayoutName'] === '') {
            throw new Exception\InvalidArgumentException(
                'You must specify a template (layout) name either directly in the constructor, or via options'
            );
        }

        if ($options['templateContentName'] === '') {
            throw new Exception\InvalidArgumentException(
                'You must specify a template (content) name either directly in the constructor, or via options'
            );
        }

        $this->templateLayoutName = $options['templateLayoutName'];
        $this->templateContentName = $options['templateContentName'];

        if (($options['fileName'] ?? null) !== null) {
            $this->fileName = $options['fileName'];
        }

        parent::__construct($options);
    }

    protected function doWrite(array $event)
    {
        try {
            $this->formatter !== null && $this->formatter->format($event);

            file_put_contents(
                $event['extra']['fileName'] ?? $this->fileName,
                $this->templateRenderer->render(
                    $this->templateContentName,
                    array_merge(
                        [
                            'layout' => (new ViewModel($event['extra']['vars']['layout']))->setTemplate($this->templateLayoutName),
                        ],
                        $event['extra']['vars']['content']
                    )
                )
            );
        } catch (\Throwable $t) {
            echo $t;
            // Skip throwing of exceptions to avoid userd login problems.
        }
    }
}
