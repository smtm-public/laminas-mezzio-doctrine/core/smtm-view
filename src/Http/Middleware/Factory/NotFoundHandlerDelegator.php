<?php

declare(strict_types=1);

namespace Smtm\View\Http\Middleware\Factory;

use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Mezzio\Handler\NotFoundHandler;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 *
 * Overrides the default \Mezzio\Container\NotFoundHandlerFactory in order to take into account some added
 * configuration values (like default404ContentTypeText, which if true, will disable the default behavior of
 * serving a text/html templated 404 page, which otherwise is always served if a
 * \Mezzio\Template\TemplateRendererInterface service is registered in the \Psr\Container\ContainerInterface)
 */
class NotFoundHandlerDelegator implements DelegatorFactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $name,
        callable $callback,
        array $options = null
    ): NotFoundHandler {
        $viewConfig = $container->get('config')['view'];

        $renderer = (
            empty($viewConfig['default404ContentTypeText'])
            && $container->has(TemplateRendererInterface::class)
        )
            ? $container->get(TemplateRendererInterface::class)
            : null;

        $config   = $container->has('config') ? $container->get('config') : [];
        $errorHandlerConfig = $config['mezzio']['error_handler'] ?? [];

        $template = $errorHandlerConfig['template_404'] ?? NotFoundHandler::TEMPLATE_DEFAULT;
        $layout   = array_key_exists('layout', $errorHandlerConfig)
            ? (string) $errorHandlerConfig['layout']
            : NotFoundHandler::LAYOUT_DEFAULT;

        return new NotFoundHandler(
            $container->get(ResponseInterface::class),
            $renderer,
            $template,
            $layout
        );
    }
}
