<?php

declare(strict_types=1);

namespace Smtm\View;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'view' => 'array',
        'templates' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'view' => include __DIR__ . '/../config/view.php',
            'templates' => include __DIR__ . '/../config/templates.php',
        ];
    }
}
